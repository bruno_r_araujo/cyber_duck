<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::group(['middleware' => 'auth'], function () {
    Route::get('/', function () {
        return redirect()->route("admin_dashboard");
    });
});

Route::group(['prefix' => 'admin', 'middleware' => 'auth'], function () {
    Route::get("/", function(){
        return view("admin/admin_template");
    })->name("admin_dashboard");

    //Company routes
    Route::get('company', "Admin\CompanyController@index")->name("company_index");
    Route::get("company/add", "Admin\CompanyController@create")->name("company_create");
    Route::get("company/{company}", "Admin\CompanyController@show")->name("company_show");
    Route::post("company", "Admin\CompanyController@store")->name("company_store");
    Route::get("company/edit/{company}", "Admin\CompanyController@edit")->name("company_edit");
    Route::put("company/{company}", "Admin\CompanyController@update")->name("company_update");
    Route::delete("company/{company}", "Admin\CompanyController@destroy")->name("company_delete");


    //Employee routes
    Route::get('employee', "Admin\EmployeeController@index")->name("employee_index");
    Route::get("employee/add", "Admin\EmployeeController@create")->name("employee_create");
    Route::post("employee", "Admin\EmployeeController@store")->name("employee_store");
    Route::get("employee/{employee}", "Admin\EmployeeController@show")->name("employee_show");
    Route::get("employee/edit/{employee}", "Admin\EmployeeController@edit")->name("employee_edit");
    Route::put("employee/{employee}", "Admin\EmployeeController@update")->name("employee_update");
    Route::delete("employee/{employee}", "Admin\EmployeeController@destroy")->name("employee_delete");

});


Auth::routes();
