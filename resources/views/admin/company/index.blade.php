@extends('admin.admin_template')
@section("header")
<section class="content-header">
        <h1>
          Company
          <small>index</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="{{ route('admin_dashboard') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
          <li class="active">Company</li>
        </ol>
@endsection
@section("content")
    <div class="row">
        <div class="col col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Company
                    </h3>
                <a class="btn btn-success pull-right" href="{{ route("company_create") }}">Add Company</a>
                </div>
                <div class="box-body">
                        <table class="table table-bordered">
                                <th>name</th>
                                <th>email</th>
                                <th>site</th>
                                <th></th>
                                @forelse ($companies as $company)
                                    <tr>
                                        <td>{{ $company->name }}</td>
                                        <td>{{ $company->email }}</td>
                                        <td>{{ $company->website }}</td>
                                        <td>
                                            <form method="post" action="{{ route("company_delete", $company->id) }}">
                                            <a class="btn btn-primary" href="{{ route("company_show", $company->id ) }}"><i class="fa fa-eye"></i></a>
                                                <a class="btn btn-success" href={{ route("company_edit", $company->id) }}><i class="fa fa-pencil"></i></a>
                                                @method("DELETE")
                                                @csrf
                                                <button type="submit" onclick="return confirm('Are you sure that you want to delete this company?')" class="btn btn-danger">
                                                <i class="fa fa-remove"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr align="center">
                                        <td colspan="3">
                                            Empty company
                                        </td>
                                    </tr>
                                @endforelse
                            </table>
                </div>
                <div class="box-footer">
                        {{ $companies->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
