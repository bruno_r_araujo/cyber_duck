@extends('admin.admin_template')
@section("header")
<section class="content-header">
        <h1>
          Company
          <small>index</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="{{ route('admin_dashboard') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="{{ route('company_index') }}"><i class="fa fa-dashboard"></i>List Company</a></li>
          <li class="active">Add Company</li>
        </ol>
@endsection
@section("content")
    <div class="row">
        <div class="col col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                       Create Company
                    </h3>
                </div>
                <div class="box-body">
                    <form method="post" enctype=multipart/form-data action="{{ route("company_update", $company->id) }}">
                        @method("PUT")
                        @csrf

                        <div class="form-group @if ($errors->has('name')) has-error @endif">
                            <label for="name" class="form-label">Company name</label>
                            <input type="text" value="{{ $company->name }}" name="name" class="form-control">
                            @if ($errors->has('name'))
                                <span class="help-block"> {{ $errors->first('name') }} </span>
                            @endif
                        </div>
                        <div class="form-group @if ($errors->has('website')) has-error @endif">
                            <label for="website" class="form-label">Website</label>
                            <input type="url" value="{{ $company->website }}" name="website" class="form-control">
                            @if ($errors->has('website'))
                                <span class="help-block"> {{ $errors->first('website') }}</span>
                            @endif
                        </div>
                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" value="{{ $company->email }}" name="email" class="form-control">
                            @if ($errors->has('email'))
                                <span class="help-block"> {{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="form-group @if ($errors->has('logo')) has-error @endif">
                            <label for="logo" class="form-label">Logo</label>
                            <input type="file" name="logo" >
                            @if ($errors->has('logo'))
                                <span class="help-block"> {{ $errors->first('logo') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Submit" class="btn btn-success">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
