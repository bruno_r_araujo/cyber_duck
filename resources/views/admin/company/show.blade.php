@extends('admin.admin_template')
@section("header")
<section class="content-header">
        <h1>
          Company
          <small>index</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin_dashboard') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="{{ route('company_index') }}"><i class="fa fa-dashboard"></i>List Company</a></li>
            <li class="active">{{ $company->name }}</li>
        </ol>
@endsection
@section("content")
    <div class="row">
        <div class="col col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Company
                    </h3>
                    <a class="btn btn-primary pull-right" href="{{ route("company_index") }}">Back to list</a>
                </div>
                <div class="box-body">
                    <table class="table table-bordered">
                        <th>name</th>
                        <th>email</th>
                        <th>site</th>
                        <th>Logo</th>
                        <tr>
                            <td>{{ $company->name }}</td>
                            <td>{{ $company->email }}</td>
                            <td>{{ $company->website }}</td>
                            <td>
                                @if(!empty($company->logo))
                                <img src="{{ asset("logos/".$company->logo) }}" />
                                @endif
                            </td>
                        </tr>
                        </table>
                </div>
                <div class="box-footer">

                </div>
            </div>
        </div>
    </div>
@endsection
