@extends('admin.admin_template')
@section("header")
    <section class="content-header">
        <h1>
            Employee
            <small>index</small>
        </h1>
        <ol class="breadcrumb">
            <li><a href="{{ route('admin_dashboard') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
            <li><a href="{{ route('employee_index') }}"><i class="fa fa-dashboard"></i>List Employees</a></li>
            <li class="active">{{ $employee->firstname }}</li>
        </ol>
        @endsection
        @section("content")
            <div class="row">
                <div class="col col-md-12">
                    <div class="box">
                        <div class="box-header with-border">
                            <h3 class="box-title">
                                Employee
                            </h3>
                            <a class="btn btn-primary pull-right" href="{{ route("employee_index") }}">Back to list</a>
                        </div>
                        <div class="box-body">
                            <table class="table table-bordered">
                                <th>firstname</th>
                                <th>lastname</th>
                                <th>email</th>
                                <th>phone</th>
                                <th>company</th>
                                <tr>
                                    <td>{{ $employee->firstname }}</td>
                                    <td>{{ $employee->lastname }}</td>
                                    <td>{{ $employee->email }}</td>
                                    <td>{{ $employee->phone }}</td>
                                    <td>{{ $employee->company->name }}</td>
                                </tr>
                            </table>
                        </div>
                        <div class="box-footer">

                        </div>
                    </div>
                </div>
            </div>
@endsection
