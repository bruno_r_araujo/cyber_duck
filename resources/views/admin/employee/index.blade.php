@extends('admin.admin_template')
@section("header")
<section class="content-header">
        <h1>
          Employee
          <small>index</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="{{ route('admin_dashboard') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
          <li class="active">Employee</li>
        </ol>
@endsection
@section("content")
    <div class="row">
        <div class="col col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                        Employees
                    </h3>
                <a class="btn btn-success pull-right" href="{{ route("employee_create") }}">Add Employee</a>
                </div>
                <div class="box-body">
                        <table class="table table-bordered">
                                <th>name</th>
                                <th>email</th>
                                <th>site</th>
                                <th></th>
                                @forelse ($employees as $empoyee)
                                    <tr>
                                        <td>{{ $empoyee->firstname }}</td>
                                        <td>{{ $empoyee->lastname }}</td>
                                        <td>{{ $empoyee->email }}</td>
                                        <td>{{ $empoyee->phone }}</td>
                                        <td>
                                            <form method="post" action="{{ route("employee_delete", $empoyee->id) }}">
                                            <a class="btn btn-primary" href="{{ route("employee_show", $empoyee->id ) }}"><i class="fa fa-eye"></i></a>
                                                <a class="btn btn-success" href={{ route("employee_edit", $empoyee->id) }}><i class="fa fa-pencil"></i></a>
                                                @method("DELETE")
                                                @csrf
                                                <button type="submit" onclick="return confirm('Are you sure that you want to delete this employee?')" class="btn btn-danger">
                                                <i class="fa fa-remove"></i></button>
                                            </form>
                                        </td>
                                    </tr>
                                @empty
                                    <tr align="center">
                                        <td colspan="3">
                                            Empty employee
                                        </td>
                                    </tr>
                                @endforelse
                            </table>
                </div>
                <div class="box-footer">
                        {{ $employees->links() }}
                </div>
            </div>
        </div>
    </div>
@endsection
