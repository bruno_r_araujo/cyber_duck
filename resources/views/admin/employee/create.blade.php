@extends('admin.admin_template')
@section("header")
<section class="content-header">
        <h1>
          Employee
          <small>index</small>
        </h1>
        <ol class="breadcrumb">
        <li><a href="{{ route('admin_dashboard') }}"><i class="fa fa-dashboard"></i>Dashboard</a></li>
        <li><a href="{{ route('employee_index') }}"><i class="fa fa-dashboard"></i>List Employee</a></li>
          <li class="active">Add Employee</li>
        </ol>
@endsection
@section("content")
    <div class="row">
        <div class="col col-md-12">
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">
                       Create Employee
                    </h3>
                </div>
                <div class="box-body">
                    <form method="POST" action="{{ route("employee_store") }}">
                        @csrf
                        <div class="form-group @if ($errors->has('firstname')) has-error @endif">
                            <label for="firstname" class="form-label">Firstname</label>
                            <input type="text" value="{{ old('firstname') }}" name="firstname" class="form-control">
                            @if ($errors->has('firstname'))
                                <span class="help-block"> {{ $errors->first('firstname') }} </span>
                            @endif
                        </div>
                        <div class="form-group @if ($errors->has('lastname')) has-error @endif">
                            <label for="lastname" class="form-label">Lastname</label>
                            <input type="text" value="{{ old('lastname') }}" name="lastname" class="form-control">
                            @if ($errors->has('lastname'))
                                <span class="help-block"> {{ $errors->first('lastname') }} </span>
                            @endif
                        </div>
                        <div class="form-group @if ($errors->has('email')) has-error @endif">
                            <label for="email" class="form-label">Email</label>
                            <input type="email" value="{{ old('email') }}" name="email" class="form-control">
                            @if ($errors->has('email'))
                                <span class="help-block"> {{ $errors->first('email') }}</span>
                            @endif
                        </div>
                        <div class="form-group @if ($errors->has('phone')) has-error @endif">
                            <label for="phone" class="form-label">Phone</label>
                            <input type="phone" value="{{ old('phone') }}" name="phone" class="form-control">
                            @if ($errors->has('phone'))
                                <span class="help-block"> {{ $errors->first('phone') }}</span>
                            @endif
                        </div>
                        <div class="form-group @if ($errors->has('company_id')) has-error @endif">
                            <label for="company_id" class="form-label">Company</label>
                            <select class="select2" name="company_id">
                                @foreach ($companies as $company)
                                    <option value="{{$company->id}}">{{$company->name}}</option>
                                @endforeach
                            </select>
                            @if ($errors->has('company_id'))
                                <span class="help-block"> {{ $errors->first('company_id') }}</span>
                            @endif
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Submit" class="btn btn-success">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
@endsection
@section("custom_javascript")
<script type="text/javascript">
$(document).ready(function() {
    $('.select2').select2();
});
</script>
@endsection
