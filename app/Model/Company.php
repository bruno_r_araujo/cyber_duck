<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Company extends Model
{
     /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'website', 'email', 'logo'
    ];

    /**
     * Get the employeess.
     */
    public function employees()
    {
        return $this->hasMany('App\Model\Employess');
    }
}
