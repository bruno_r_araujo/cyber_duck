<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Employee extends Model
{
    protected $fillable = [
        'firstname', 'lastname', 'email', 'phone', 'company_id'
    ];

    /**
     * Get the company.
     */
    public function company()
    {

        return $this->belongsTo('App\Model\Company');
    }
}
