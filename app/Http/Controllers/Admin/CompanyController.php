<?php

namespace App\Http\Controllers\Admin;

use App\Model\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyRequest;

class CompanyController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $companies = Company::paginate(10);
        return view("admin.company.index")
        ->with("companies", $companies);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.company.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CompanyRequest $request)
    {

        $company = $request->all();
        if ($request->has("logo")) {
            $logo = $request->file('logo');
            $extension = $logo->getClientOriginalExtension();
            $company["logo"] = $logo->getClientOriginalName();
        }

        $company = new Company($company);

        if ($company->save() ) {
            if ($request->has("logo")) {
                \Storage::disk('public')->put($company->id.'.'.$extension,  \File::get($logo));
            }

            return redirect()->route("company_index")->with("success", "Company created with success!");
        }

        return redirect()->route("company_index")->with("error", "Failure to create company, please contact the support!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function show(Company $company)
    {
        if (empty($company->id)) {
            \abort(404);
        }

        return view("admin.company.show")->with("company", $company);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function edit(Company $company)
    {
        if ( empty($company->id)) {
            abort(404);
        }

        return view("admin.company.edit")
        ->with("company", $company);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function update(CompanyRequest $request, Company $company)
    {
        $values = $request->all();

        if ($request->has("logo")) {
            $logo = $request->file('logo');
            $extension = $logo->getClientOriginalExtension();
            $values["logo"] = $logo->getClientOriginalName();
            $oldFile = $company->logo;
            \Storage::disk("public")->delete($oldFile);
            \Storage::disk('public')->put($logo->getClientOriginalName(),  \File::get($logo));
        }

        if ($company->update($values)) {
            return redirect()->route("company_index")->with("success", "Company updated with success!");
        }

        return redirect()->route("company_index")->with("error", "Failure to update company, please contact the support!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Company  $company
     * @return \Illuminate\Http\Response
     */
    public function destroy(Company $company)
    {
        $filename = $company->logo;
        if ($company->delete()) {
            Storage::disk("public")->delete($filename);
            return redirect()->route("company_index")->with("success", "Company deleted with success!");;
        }

        return redirect()->route("company_index")->with("error", "Failure to delete company, please contact the support!");
    }
}
