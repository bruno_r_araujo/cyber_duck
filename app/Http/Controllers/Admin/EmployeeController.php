<?php

namespace App\Http\Controllers\Admin;

use App\Model\Employee;
use App\Model\Company;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeRequest;

class EmployeeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("admin.employee.index")->with("employees",Employee::paginate(10));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.employee.create")->with("companies", Company::all(["id","name"]));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EmployeeRequest $request)
    {
        $employee = new Employee($request->all());
        if ($employee->save()) {
            return redirect()->route("employee_index")->with("success", "Employee created with success!");
        }

        return redirect()->route("company_index")->with("error", "Failure to create employee, please contact the support!");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Model\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function show(Employee $employee)
    {
        return view("admin.employee.show")->with("employee",$employee);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Model\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function edit(Employee $employee)
    {

        return view("admin.employee.edit")
            ->with("companies", Company::all(["id","name"]))
            ->with("employee",$employee);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Model\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function update(EmployeeRequest $request, Employee $employee)
    {
        if ($employee->update($request->all())) {
           return redirect()->route("employee_index")->with("success", "Employee updated with success!");
        }

        return redirect()->route("company_index")->with("error", "Failure to update employee, please contact the support!");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Model\Employee  $employee
     * @return \Illuminate\Http\Response
     */
    public function destroy(Employee $employee)
    {
        if ($employee->delete()) {
            return redirect()->route("employee_index")->with("success", "Employee deleted with success!");
        }

        return redirect()->route("company_index")->with("error", "Failure to delete employee, please contact the support!");
    }
}
